#
# Cookbook:: mynginx
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
package 'nginx' do
  action :remove
end

package 'apache2' do
  action :install
end

service 'apache2' do
  action [ :enable, :start ]
end
